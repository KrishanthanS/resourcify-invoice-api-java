package com.example.natifai.service;

import org.springframework.core.io.ByteArrayResource;

class PdfMultipartFileResource extends ByteArrayResource {

    private final String fileName;

    public PdfMultipartFileResource(byte[] pdfBytes) {
        super(pdfBytes);
        this.fileName = "file.pdf";
    }

    @Override
    public String getFilename() {
        return this.fileName;
    }

    @Override
    public String getDescription() {
        return "Multipart PDF file resource [" + getFilename() + "]";
    }
}
