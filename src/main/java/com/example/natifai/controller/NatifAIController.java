package com.example.natifai.controller;

import com.example.natifai.service.NatifAiInvoiceExtractionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class NatifAIController {

    private final NatifAiInvoiceExtractionService natifAiInvoiceExtractionService;

    @GetMapping("/invoiceItems")
    public String extractInvoiceItems() {
        return natifAiInvoiceExtractionService.extractInvoiceItems();
    }

}
