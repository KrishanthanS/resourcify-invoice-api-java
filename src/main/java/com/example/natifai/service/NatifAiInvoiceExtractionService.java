package com.example.natifai.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.nio.file.Files;

import static java.util.Collections.singletonList;
import static org.springframework.http.MediaType.*;

@RequiredArgsConstructor
@Slf4j
@Service
public class NatifAiInvoiceExtractionService {

    private final RestTemplate restTemplate;
    private NatifaiToken token;

    public String extractInvoiceItems() {

        final var requestEntity = createRequestEntity();

        if (requestEntity == null) {
            log.info("Request entity is null");
            return null;
        }

        final var response = restTemplate
                .postForEntity("https://api.natif.ai/processing/babcc341-4e4f-475c-a5a1-0307ce248ed3", requestEntity, String.class);

        return response.getBody();
    }

    private HttpEntity<MultiValueMap<String, Object>> createRequestEntity() {

        final var httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MULTIPART_FORM_DATA);
        httpHeaders.setAccept(singletonList(APPLICATION_JSON));
        httpHeaders.set("Authorization", "Bearer " + createToken().getAccessToken());

        byte[] bytes;
        try {
            File file = ResourceUtils.getFile("classpath:kng_1.pdf");
            bytes = Files.readAllBytes(file.toPath());
        } catch (Exception e) {
            log.error("Error encountered during invoice pdf initialization: {}", e.getMessage());
            return null;
        }

        final MultiValueMap<String, Object> body
                = new LinkedMultiValueMap<>();
        body.add("file", new PdfMultipartFileResource(bytes));

        return new HttpEntity<>(body, httpHeaders);
    }

    private NatifaiToken createToken() {
        if (token == null || token.isExpired()) {
            log.info("Requesting new JWT token from NatifAI");

            final var httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(APPLICATION_FORM_URLENCODED);
            httpHeaders.setAccept(singletonList(APPLICATION_JSON));

            final MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
            //body.add("username", "pascal.alich@resourcify.de");
            body.add("username", "krishanthan.somasundaram@resourcify.com");

            //body.add("password", "7l7#,Mgj/i#@]8");
            body.add("password", "7Jkso03d#dkow7304");

            final HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(body, httpHeaders);

            final var response =
                    new RestTemplate().postForEntity("https://api.natif.ai/" + "token", requestEntity, NatifaiToken.class);
            token = response.getBody();
        }
        return token;
    }
}
