## For local development
```
apache-maven-3.9.2
JDK 17.0.3-zulu
```

## how to build
```
mvn clean install
```

## how to run

```
run NatifAIApplication.java
visit http://localhost:8080/invoiceItems to see the extracted invoice items
```

## About NatifAI API
```
The basic API that NatifAI provides is
https://api.natif.ai/processing/invoice_extraction

but the extracted data is not precise enough so we trained the model using certain invoice pdfs.
After the training had been completed NatifAI provides a custom API
https://api.natif.ai/processing/babcc341-4e4f-475c-a5a1-0307ce248ed3
Using this API we were able to get accurate results on invoice items
```