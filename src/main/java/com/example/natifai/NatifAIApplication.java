package com.example.natifai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NatifAIApplication {

	public static void main(String[] args) {
		SpringApplication.run(NatifAIApplication.class, args);
	}

}
