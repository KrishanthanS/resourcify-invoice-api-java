package com.example.natifai.service;


import static java.lang.Long.parseLong;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.time.Instant;
import java.util.Base64;
import java.util.HashMap;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Getter
@Setter
class NatifaiToken {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @JsonProperty("access_token")
    private String accessToken;
    @JsonProperty("token_type")
    private String tokenType;

    public boolean isExpired() {
        if (!StringUtils.hasText(accessToken)) {
            return true;
        }

        String encodedPayload = accessToken.split("\\.")[1];
        String payload = new String(Base64.getDecoder().decode(encodedPayload));
        try {
            String exp = OBJECT_MAPPER.readValue(payload, HashMap.class).get("exp").toString();
            return Instant.now().isAfter(Instant.ofEpochSecond(parseLong(exp)).minusSeconds(60L));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

